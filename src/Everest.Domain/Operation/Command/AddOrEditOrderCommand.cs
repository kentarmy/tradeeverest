﻿using Microsoft.AspNetCore.Http;

namespace Everest.Domain;

#region << Using >>

using FluentValidation;
using Incoding.Core.CQRS.Core;
using  Everest.Domain;
using static Everest.Domain.Order;

#endregion


public class AddOrEditOrderCommand : CommandBase
{
    public int Id { get; set; }
    public string ClientEmail { get; set; }
    public string ClientName { get; set; }
    public string? ClientComment { get; set; }
    public string? EmployeeComment { get; set; }
    
    public OfStatus Status { get; set; }
    public User User { get; set; }


    protected override void Execute()
    {
        var order = Repository.GetById<Order>(Id);
        
        // Заполнение полей заказа данными из команды
        order.ClientEmail = ClientEmail;
        order.ClientName = ClientName;
        order.ClientComment = ClientComment;
        order.EmployeeComment = EmployeeComment;
        order.Status = Status;
        order.User = User;
        Repository.SaveOrUpdate(order);
    }

    public class Validator : AbstractValidator<AddOrEditOrderCommand>
    {
        public Validator()
        {
            
        }
    }
}
