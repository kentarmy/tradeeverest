﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Incoding.Core.CQRS.Core;
using Incoding.Web.MvcContrib;
using Newtonsoft.Json.Linq;
using NHibernate.Criterion;
using NHibernate.Engine;


namespace Everest.Domain;
public class DeleteOrderCommand : CommandBase
{
    public int Id { get; set; }

    protected override void Execute()

    {
        Repository.Delete(Repository.GetById<Order>(Id));
    }
}

public class EditOrderCommand : CommandBase
{
    public int Id { get; set; }
    public string EmployeeComment { get; set; }
    public Order.OfStatus Status { get; set; }
    public int EmployeeId { get; set; }

    protected override void Execute()
    {
        var order = Repository.GetById<Order>(Id);


        // Заполнение полей заказа данными из команды
        
        order.EmployeeComment = EmployeeComment;
        order.Status = Status;
        order.Employee = Repository.GetById<Employee>(EmployeeId);
        Repository.SaveOrUpdate(order);
    }

    public class Validator : AbstractValidator<EditOrderCommand>
    {
        public Validator()
        {
            RuleFor(q => q.EmployeeId).NotEmpty();
        }
    }
    public class AsQuery : QueryBase<EditOrderCommand>
    {
        public int Id { get; set; }

        protected override EditOrderCommand ExecuteResult()
        {
            var order = Repository.LoadById<Order>(Id);

            return new EditOrderCommand
            {
                Id = order.Id,
                EmployeeComment = order.EmployeeComment, // Устанавливаем комментарий менеджера
                Status = order.Status,
            };
        }
    }
}
 