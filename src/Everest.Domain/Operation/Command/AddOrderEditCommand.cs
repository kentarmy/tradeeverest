﻿using System;
using System.Collections.Generic;
using FluentValidation;
using Incoding.Core.CQRS.Core;
using Incoding.Web.MvcContrib;
using static Everest.Domain.Order;

namespace Everest.Domain;

public class AddEditOrderCommand : CommandBase
{
    public int? Id { get; set; }
    public string ClientName { get; set; }
    public string ClientEmail { get; set; }
    public string ClientComment { get; set; } // Комментарий от клиента
    public string EmployeeComment { get; set; } // Комментарий от менеджера
    public OfStatus Status { get; set; }
    public DateTime OrderDate { get; set; }
    public List<OrderItem> OrderItems { get; set; }

    protected override void Execute()
    {
        var isNew = !Id.HasValue || Id == 0;
        Order order = isNew ? new Order() : Repository.GetById<Order>(Id.Value);

        order.ClientName = ClientName;
        order.ClientEmail = ClientEmail;
        order.ClientComment = ClientComment;
        order.EmployeeComment = EmployeeComment; // Сохраняем комментарий менеджера
        order.Status = Status;
        order.OrderDate = OrderDate;

        Repository.SaveOrUpdate(order);
    }

    public class Validator : AbstractValidator<AddEditOrderCommand>
    {
        public Validator()
        {
            RuleFor(q => q.ClientName).NotEmpty();
            RuleFor(q => q.ClientEmail).NotEmpty();
            RuleFor(q => q.Status).NotEmpty();
            RuleFor(q => q.OrderDate).NotEmpty();
        }
    }

    public class AsQuery : QueryBase<AddEditOrderCommand>
    {
        public int Id { get; set; }

        protected override AddEditOrderCommand ExecuteResult()
        {
            var order = Repository.LoadById<Order>(Id);

            return new AddEditOrderCommand
            {
                Id = order.Id,
                ClientName = order.ClientName,
                ClientEmail = order.ClientEmail,
                ClientComment = order.ClientComment,
                EmployeeComment = order.EmployeeComment, // Устанавливаем комментарий менеджера
                Status = order.Status,
                OrderDate = order.OrderDate,
                OrderItems = order.OrderItems.ToList()
            };
        }
    }
}
