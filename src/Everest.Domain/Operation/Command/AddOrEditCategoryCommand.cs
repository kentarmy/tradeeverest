﻿using FluentValidation;
using Incoding.Core.CQRS.Core;

namespace Everest.Domain;

    public class DeleteCategoryCommand : CommandBase
    {
        public int Id { get; set; }

    protected override void Execute()
    {
        var category = Repository.GetById<Category>(Id);
        if (category == null)
            throw new ArgumentException($"Category with Id {Id} not found.");

        // Check for dependent products
        var dependentProducts = Repository.Query<Product>().Where(p => p.Category.Id == Id).ToList();
        if (dependentProducts.Any())
            throw new InvalidOperationException("Cannot delete category with dependent products.");

        Repository.Delete(category);
    }
}

    public class AddOrEditCategoryCommand : CommandBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Category.OfType Type { get; set; }

        protected override void Execute()
        {
            var isNew = Id == 0;
            Category ca = isNew ? new Category() : Repository.GetById<Category>(Id);
            ca.Name = Name;
            ca.Type = Type;

            Repository.SaveOrUpdate(ca);
        }

        public class Validator : AbstractValidator<AddOrEditCategoryCommand>
        {
            public Validator()
            {
                RuleFor(ca => ca.Name).NotEmpty();
            }
        }

        public class AsQuery : QueryBase<AddOrEditCategoryCommand>
        {
            public int Id { get; set; }

            protected override AddOrEditCategoryCommand ExecuteResult()
            {
                var ca = Repository.GetById<Category>(Id);
                if (ca == null)
                    return new AddOrEditCategoryCommand();

                return new AddOrEditCategoryCommand()
                {
                    Id = ca.Id,
                    Name = ca.Name,
                    Type = ca.Type,
                };
            }
        }
    }
