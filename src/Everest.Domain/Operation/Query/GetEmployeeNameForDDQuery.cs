﻿using Incoding.Core.CQRS.Core;
using Incoding.Core.Data;
using Incoding.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Everest.Domain;

public class GetEmployeeNameForDDQuery : QueryBase<List<KeyValueVm>>
{

    protected override List<KeyValueVm> ExecuteResult()
    {
        var keyValueVms = Repository.Query<Employee>()
            .Select(q => new KeyValueVm(q.Id, $"{q.FirstName} {q.LastName}"))
            .ToList();

        keyValueVms.Insert(0, new KeyValueVm("", "Все сотрудники"));
        return keyValueVms;
    }
}