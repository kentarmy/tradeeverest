﻿using Incoding.Core.CQRS.Core;
using Incoding.Core.Data;
using Incoding.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Everest.Domain
{
    public class GetOrderStatusForDDQuerys : QueryBase<List<KeyValueVm>>
    {
        protected override List<KeyValueVm> ExecuteResult()
        {
            var keyValueVms = new List<KeyValueVm>();

            var statusMappings = new Dictionary<Order.OfStatus, string>
            {
                { Order.OfStatus.New, "Новый" },
                { Order.OfStatus.Processing, "В обработке" },
                { Order.OfStatus.Canceled, "Отменен" },
                { Order.OfStatus.Completed, "Завершен" },
                
            };
            foreach (var mapping in statusMappings)
            {
                keyValueVms.Add(new KeyValueVm(mapping.Key.ToString(), mapping.Value));
            }

            keyValueVms.Insert(0, new KeyValueVm("", "Все статусы"));
            return keyValueVms;
        }
    }
}