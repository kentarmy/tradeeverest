﻿#region << Using >>

using Incoding.Core.Block.Caching;
using Incoding.Core.CQRS.Core;
using Incoding.Core.Extensions;
using System.Runtime.InteropServices.Marshalling;

#endregion

namespace Everest.Domain;

    public class GetSetting : QueryBase<GetSetting.Response>
{
    public class Response
    {
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }

    protected override Response ExecuteResult()
    {
        var response = new Response
        {
            SettingName = "ExampleSetting",
            SettingValue = "Value"
        };
        return response;
    }
    public class IsAccessToAdminSettingQuery : QueryBase<bool>
    {
        public string? Password { get; set; }
        public string? Login { get; set; }

        protected override bool ExecuteResult()
        {
            const string adminLogin = "admin";
            const string adminPassword = "everest";

            if (Login == null || Password == null)
            {
                return false; 
            }

            if (Login.Equals(adminLogin, StringComparison.OrdinalIgnoreCase))
            {
                
                if (Password != null && Password.Equals(adminPassword, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }
}

