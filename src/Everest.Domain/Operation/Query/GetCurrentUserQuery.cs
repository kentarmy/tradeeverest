﻿using Incoding.Core.Block.IoC;
using Incoding.Core.CQRS.Core;
using Microsoft.AspNetCore.Http;

namespace Everest.Domain;
public class GetCurrentUserQuery : QueryBase<GetCurrentUserQuery.Response>
{
    public class Response
    {
        public int Id { get; set; }
    }

    protected override Response ExecuteResult()
    {
        var httpContext = IoCFactory.Instance.TryResolve<IHttpContextAccessor>().HttpContext;
        var cookieKey = "UserId";
        var tempId = httpContext.Request.Cookies[cookieKey];
        if (string.IsNullOrWhiteSpace(tempId))
        {
            tempId = Guid.NewGuid().ToString();
            httpContext.Response.Cookies.Append(cookieKey, tempId, new CookieOptions
            {
                HttpOnly = true,   // Куки доступны только через HTTP(S), недоступны через JavaScript
                Secure = true,     // Куки отправляются только по HTTPS
                SameSite = SameSiteMode.Strict, // Куки не отправляются с кросс-сайтовыми запросами
                Expires = DateTimeOffset.UtcNow.AddYears(1) // Срок действия куки - 1 год
            });
            Dispatcher.New().Push(new NewUserCommand() { TempId = tempId });
        }

        var userId = Repository.Query<User>().Single(q => q.TempId == tempId).Id;
        return new Response() { Id = userId };
    }
}

public class NewUserCommand : CommandBase
{
    protected override void Execute()
    {
        Repository.Save(new User() { TempId = TempId });
    }

    public string TempId { get; set; }
}
