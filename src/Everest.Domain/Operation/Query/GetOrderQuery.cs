﻿using Incoding.Core.CQRS.Core;

namespace Everest.Domain;

public class GetOrderQuery : QueryBase<List<GetOrderQuery.Response>>
{
    
    public class Response
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string ClientComment { get; set; }
        public string ClientEmail { get; set; }
        public string ClientName { get; set; }
        public string OrderDate { get; set; }
        public string EmployeeComment { get; set; }
        public int? EmployeeId { get; set; }
        public string? EmployeeName { get; set; }
        public string ProductNameString { get; set; }
        public int CountProducts { get; set; }

    }
    public int? Search { get; set; }
    public Order.OfStatus? SearchStatus { get; set; } 
    public int? EmployeeId { get; set; }
    protected override List<Response> ExecuteResult()
    {
        var statusMappings = new Dictionary<Order.OfStatus, string>
        {
            { Order.OfStatus.New, "Новый" },
            { Order.OfStatus.Processing, "В обработке" },
            { Order.OfStatus.Canceled, "Отменен" },
            { Order.OfStatus.Completed, "Завершен" },
        };
        IQueryable<Order> query = Repository.Query<Order>()
            .OrderByDescending(q => q.OrderDate); // Сортируем по времени заказа в обратном порядке (сначала новые)
        if (Search.HasValue)
        {
            query = query.Where(q => q.Id == Search.Value);
        }
        if (SearchStatus.HasValue)
        {
            query = query.Where(q => q.Status == SearchStatus.Value);
        }
        if (EmployeeId.HasValue)
        {
            query = query.Where(q => q.Employee.Id == EmployeeId.Value);
        }
        return query
            .ToList()
            .Select(q => new Response()
            {

                Id = q.Id,
                ClientName = q.ClientName,
                Status = statusMappings[q.Status],
                ClientComment = q.ClientComment,
                ClientEmail = q.ClientEmail,
                OrderDate = q.OrderDate.ToString("dd.MM.yyyy HH:mm"),
                EmployeeComment = q.EmployeeComment,
                EmployeeId = q.Employee != null ? q.Employee.Id : null,  

                EmployeeName = q.Employee != null ? $"{q.Employee.FirstName} {q.Employee.LastName}" : null,
                ProductNameString = string.Join("; ", q.OrderItems.Select(item => item.Product.ProductName)),
                CountProducts = q.OrderItems.Count()

            })
            .ToList();
    }
}



