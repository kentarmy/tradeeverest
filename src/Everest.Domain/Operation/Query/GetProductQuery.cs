﻿namespace Everest.Domain;

#region << Using >>

using Incoding.Core.CQRS.Core;

#endregion

public class GetProductQuery : QueryBase<List<GetProductQuery.Response>>
{
    

    public int? Id { get; set; }
    public string Search { get; set; }
    public int? CategoryId { get; set; }
    public int? Count { get; set; }

    protected override List<Response> ExecuteResult()
    {
        var currentUser = Dispatcher.Query(new GetCurrentUserQuery()).Id;
        var allInCart = Repository.Query<CartItem>()
                                  .Where(q => q.Cart.User.Id == currentUser)
                                  .Select(q => new
                                  {
                                      ProductId = q.Product.Id,
                                      CartItemId = q.Id
                                  })
                                  .ToArray();

        Search ??= string.Empty;
        return Repository.Query<Product>()
                         .Where(e => string.IsNullOrWhiteSpace(Search) || e.ProductName.ToLower().Contains(Search.ToLower()))
                         .Where(e => !CategoryId.HasValue || e.Category.Id == CategoryId.GetValueOrDefault())
                         .ToList()
                         .Select(q => new Response
                         {
                             Id = q.Id,
                             ProductName = q.ProductName,
                             LongDescription = q.LongDescription,
                             ShortDescription = q.ShortDescription,
                             ProductPhoto = q.ProductPhoto,
                             CategoryName = q.Category.Name,

                             CartItemId = allInCart.FirstOrDefault(arg => arg.ProductId == q.Id)?.CartItemId
                         })
                         .Take(Count.GetValueOrDefault() == 0 ? int.MaxValue : Count.GetValueOrDefault())
                         .ToList();
    }

    public class Response
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int? CartItemId { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        public byte[] ProductPhoto { get; set; }
        public string CategoryName { get; set; }

    }


}