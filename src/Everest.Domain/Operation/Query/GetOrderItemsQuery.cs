﻿using Incoding.Core.CQRS.Core;

namespace Everest.Domain;

public class GetOrderItemsQuery : QueryBase<List<GetOrderItemsQuery.Response>>
{
    public class Response
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }

    public int Id { get; set; }

    protected override List<Response> ExecuteResult()
    {
        var orderItems = Repository.Query<OrderItem>()
            .Where(oi => oi.Order.Id == Id)
            .ToList();

        return orderItems.Select(item => new Response
        {
            Id = item.Order.Id,
            ProductId = item.Product.Id,
            ProductName = item.Product.ProductName,
            
        }).ToList();
    }
}
