﻿using Incoding.Core.CQRS.Core;

namespace Everest.Domain;

public class GetCountCartQuery:QueryBase<int>
{
    protected override int ExecuteResult()
    {
        var currentUser = Dispatcher.Query(new GetCurrentUserQuery()).Id;
        return Repository
            .Query<CartItem>()
            .Count(q => q.Cart.User.Id == currentUser);
    }
}

public class GetCartItemsByCurrentUserQuery : QueryBase<List<GetCartItemsByCurrentUserQuery.Response>>
{
    public class Response
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Product { get; set; }
        public int ProductId { get; set; }
        public string CategoryName { get; set; }
      
    }
    protected override List<Response> ExecuteResult()
    {
        var currentUser = Dispatcher.Query(new GetCurrentUserQuery()).Id;
        return Repository.Query<CartItem>()
            .Where(q => q.Cart.User.Id == currentUser) 
            .Select(q => new Response()
            {
                Id = q.Id,
                UserId = q.Cart.User.Id,
                Product = q.Product.ProductName,
                ProductId = q.Product.Id,
                CategoryName = q.Product.Category.Name,
            })
            .ToList();
    }
}

