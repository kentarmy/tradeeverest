﻿
using Incoding.Data.NHibernate;

#region << Using >>

using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using Incoding.Core.Extensions.LinqSpecs;
using Incoding.Core.Quality;
using JetBrains.Annotations;

#endregion
namespace Everest.Domain;

public class SettingsRole : EverestEntityBase
{
    public virtual string RoleId { get; set; }
    [Description("Access to Admin")]
    public virtual string RoleToolServerSettings { get; set; }
    public virtual string Notes { get; set; }


    [UsedImplicitly, Obsolete(ObsoleteMessage.ClassNotForDirectUsage, true), ExcludeFromCodeCoverage]
    public class Map : NHibernateEntityMap<SettingsRole>
    {
        public Map()
        {
            Id(s => s.RoleId, "settingsRoleID").GeneratedBy.Assigned();
            Map(s => s.RoleToolServerSettings, "settingsRoleToolServerSettings");
            Map(r => r.Notes, "settingsRoleNotes");
        }
    }
    public class Where
    {
        public class HaveRoleId : Specification<SettingsRole>
        {
            private readonly List<string> roles;

            public HaveRoleId(List<string> roles)
            {
                this.roles = roles.Select(q => q.ToUpper()).ToList();
            }

            public override Expression<Func<SettingsRole, bool>> IsSatisfiedBy()
            {
                return role => this.roles.Contains(role.RoleId.ToUpper());
            }
        }
    }
}
