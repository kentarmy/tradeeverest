﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Incoding.Core.Quality;
using Incoding.Data.NHibernate;
using JetBrains.Annotations;
using NHibernate.Mapping;


namespace Everest.Domain;

public class Order : EverestEntityBase
{
    public enum OfStatus
    {
        New,
        Processing,
        Canceled,
        Completed,
    }
    public virtual OfStatus Status { get; set; }
    public virtual DateTime OrderDate { get; set; }
    [MaxLength(1000)]
    public virtual string ClientComment { get; set; }
    public virtual string ClientEmail { get; set; }
    public virtual string ClientName { get; set; }
    public virtual string ClientPhone { get; set; }
    public virtual User User { get; set; }
    public virtual IList<OrderItem> OrderItems { get; set; }
    public virtual Employee Employee { get; set; }
    [MaxLength(1000)]
    public virtual string EmployeeComment { get; set; }


    [UsedImplicitly, Obsolete(ObsoleteMessage.ClassNotForDirectUsage, true), ExcludeFromCodeCoverage]
    public class Map : NHibernateEntityMap<Order>
    {
        public Map()
        {

            Id(o => o.Id).GeneratedBy.Identity();
            MapEscaping(o => o.ClientComment);
            MapEscaping(o => o.ClientEmail);
            MapEscaping(o => o.OrderDate);
            MapEscaping(o => o.ClientName);
            MapEscaping(o => o.ClientPhone);
            MapEscaping(o => o.Status).CustomType<OfStatus>();
            MapEscaping(o => o.EmployeeComment);
            References(o => o.User);
            References(o => o.Employee);
            HasMany(o => o.OrderItems)
                .Cascade.All() // Указываем каскадное удаление
                .Inverse(); // Обратное отображение

        }
    }
}

public class OrderItem : EverestEntityBase
{
    public virtual Order Order { get; set; }
    public virtual Product Product { get; set; }


    [UsedImplicitly, Obsolete(ObsoleteMessage.ClassNotForDirectUsage, true), ExcludeFromCodeCoverage]
    public class Map : NHibernateEntityMap<OrderItem>
    {
        public Map()
        {
            Id(o => o.Id).GeneratedBy.Identity();
            References(o => o.Product);
            References(o => o.Order);
        }
    }
}