$(document).ready(function() {
    $('#carousel-prev').on('click', function(ev) {
        if ($(this).attr('disabled') == undefined)
            carouselScroll($('#carousel-container'), $('#carousel-prev, #carousel-next'))
    })

    $('#carousel-next').on('click', function(ev) {
        if ($(this).attr('disabled') == undefined)
            carouselScroll($('#carousel-container'), $('#carousel-prev, #carousel-next'), true)
    })

    $('#products-left').on('click', function(ev) {
        if ($(this).attr('disabled') == undefined)
            carouselScroll($('#products'), $('#products-left, #products-right'))
    })

    $('#products-right').on('click', function(ev) {
        if ($(this).attr('disabled') == undefined)
            carouselScroll($('#products'), $('#products-left, #products-right'), true)
    })
})
// ������ ��� ������� ���������
document.querySelectorAll('.nav-link').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        const targetId = this.getAttribute('href').substring(1);
        const targetElement = document.getElementById(targetId);
        if (targetElement) {
            targetElement.scrollIntoView({
                behavior: 'smooth'
            });
        }
    });
});

// ������ ��� ��������� ��������� �������� ���������
window.addEventListener('scroll', () => {
    let fromTop = window.scrollY + document.querySelector('.navbar').offsetHeight + 10;

    document.querySelectorAll('.nav-link').forEach(link => {
        let section = document.querySelector(link.getAttribute('href'));

        if (
            section.offsetTop <= fromTop &&
            section.offsetTop + section.offsetHeight > fromTop
        ) {
            link.classList.add('active');
        } else {
            link.classList.remove('active');
        }
    });
});
function carouselScroll(container, buttons, isForward = false) {
    var itemWidth = container.children().first().outerWidth()
    
    if (isForward == false)
        itemWidth *= -1

    var value = container.scrollLeft() + itemWidth

    buttons.attr('disabled', true)
    setTimeout(() => {
        buttons.removeAttr('disabled', '')
    }, 500);

    container.animate({ scrollLeft: value }, 500)
}
document.addEventListener('DOMContentLoaded', function () {
    let lazyImages = document.querySelectorAll('.lazy-image');
    let lazySections = document.querySelectorAll('.lazy-section');
    let options = {
        root: null,
        rootMargin: '0px',
        threshold: 0.1
    };

    let observer = new IntersectionObserver(function (entries, observer) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                let img = entry.target.querySelector('.lazy-image');
                if (img) {
                    img.src = img.dataset.src;
                    observer.unobserve(entry.target);
                }
            }
        });
    }, options);

    lazySections.forEach(section => {
        observer.observe(section);
    });
});

document.addEventListener("DOMContentLoaded", function () {
    const navLinks = document.querySelectorAll('.navbar-nav .nav-link');

    navLinks.forEach(link => {
        link.addEventListener('click', function () {
            navLinks.forEach(item => item.classList.remove('active'));
            this.classList.add('active');
        });
    });
});

document.addEventListener("DOMContentLoaded", function () {
    let currentStartIndex = 0;

    function adjustCardVisibility() {
        var cards = document.querySelectorAll('.card');
        var containerWidth = document.querySelector('.section').offsetWidth;
        var cardWidth = 300 + 20; // ������ �������� + ������
        var maxCards = Math.floor(containerWidth / cardWidth);

        cards.forEach((card, index) => {
            if (index >= currentStartIndex && index < currentStartIndex + maxCards) {
                card.style.display = 'block';
            } else {
                card.style.display = 'none';
            }
        });

        document.getElementById('prevButton').disabled = currentStartIndex === 0;
        document.getElementById('nextButton').disabled = currentStartIndex + maxCards >= cards.length;
    }

    function showNextCards() {
        var cards = document.querySelectorAll('.card');
        var containerWidth = document.querySelector('.section').offsetWidth;
        var cardWidth = 300 + 20; // ������ �������� + ������
        var maxCards = Math.floor(containerWidth / cardWidth);

        if (currentStartIndex + maxCards < cards.length) {
            currentStartIndex += maxCards;
        } else {
            currentStartIndex = 0; // ������� � ������ ������
        }

        adjustCardVisibility();
    }

    function showPreviousCards() {
        var cards = document.querySelectorAll('.card');
        var containerWidth = document.querySelector('.section').offsetWidth;
        var cardWidth = 300 + 20; // ������ �������� + ������
        var maxCards = Math.floor(containerWidth / cardWidth);

        if (currentStartIndex - maxCards >= 0) {
            currentStartIndex -= maxCards;
        } else {
            currentStartIndex = 0; // ������� � ������ ������
        }

        adjustCardVisibility();
    }

    document.getElementById('nextButton').addEventListener('click', showNextCards);
    document.getElementById('prevButton').addEventListener('click', showPreviousCards);

    adjustCardVisibility();

    setInterval(showNextCards, 4000); // ����� �������� ������ 4 �������

    window.addEventListener('resize', adjustCardVisibility);
});